from kubernetes import client, watch, config

import os

if os.path.exists('/var/run/secrets/kubernetes.io/serviceaccount/token'):
    config.load_incluster_config()
else:
    try:
        config.load_kube_config()
    except:
        print('Unable to find cluster configuration')
        exit(1)

w = watch.Watch()
v1 = client.CoreV1Api()
for ns in w.stream(v1.list_namespace):
    #Le code suivant va être exécuté à chaque namespace détecté, puis à chaque évènement touchant un namespace
    obj = ns['object']
    print("{}: {}".format(ns['type'], obj.metadata.name))


