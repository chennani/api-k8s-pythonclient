from kubernetes import client, config

config.load_kube_config()

appsv1 = client.AppsV1Api()

all_deploy = appsv1.list_deployment_for_all_namespaces()
print('UNAVAILABLE DEPLOYMENTS')
for deploy in all_deploy.items:
    if not deploy.status.ready_replicas:
        print(deploy.metadata.name)
    
