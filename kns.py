from kubernetes import client, config

# Load the connection config, default behaviour is to load ~/.kube/config
# For development this is fine, when moving into production another way
# of providing the config is required
config.load_kube_config()

v1 = client.CoreV1Api()
nameSpaceList = v1.list_namespace()
for nameSpace in nameSpaceList.items:
    print(nameSpace.metadata.name)

