from kubernetes import client, config
import os

if os.path.exists('/var/run/secrets/kubernetes.io/serviceaccount/token'):
    config.load_incluster_config()
else:
    try:
        config.load_kube_config()
    except:
        print('Unable to find cluster configuration')
        exit(1)

v1 = client.CoreV1Api()
all_pods = v1.list_pod_for_all_namespaces()
res = {}
for pod in all_pods.items:
    pod_ns = pod.metadata.namespace
    pod_name = pod.metadata.name
    for container in pod.status.container_statuses:
        cont_name = container.name
        if container.restart_count:
            if not pod_ns in res:
              res[pod_ns] = {}
            if not pod_name in res[pod_ns]:
              res[pod_ns][pod_name] = {}
            res[pod_ns][pod_name][container.name] = container.restart_count

if not res: 
    print('No container has restarted')
    exit(0)
listformat="{0:<16}{1:<64}{2:>8} {3:<32}"
print(listformat.format('NAMESPACE','POD','RESTARTS','CONTAINER'))
for ns in res:
    print(listformat.format(ns,'','',''))
    for pod in res[ns]:
        print(listformat.format('',pod,'',''))
        for container in res[ns][pod]:
            print(listformat.format('','',container,res[ns][pod][container]))

