from kubernetes import client, config
import os

if os.path.exists('/var/run/secrets/kubernetes.io/serviceaccount/token'):
    config.load_incluster_config()
else:
    try:
        config.load_kube_config()
    except:
        print('Unable to find cluster configuration')
        exit(1)

v1 = client.CoreV1Api()

#all_pods = v1.list_namespaced_pod(namespace="kube-system")
all_pods = v1.list_pod_for_all_namespaces()
#ns_list = v1.list_namespace()

listformat="{:.<64}{:16}{:16}"
print(listformat.format('POD NAME', 'IP', 'Node Name'))

for pod in all_pods.items:
    pod_name = pod.metadata.name
    pod_ip = pod.status.pod_ip
    node_name = pod.spec.node_name
    print(listformat.format(pod_name, pod_ip, node_name))

